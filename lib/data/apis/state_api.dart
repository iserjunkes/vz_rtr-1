import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class StateApi {
  static Future<List<CountryState>> getStates(Base base) async {
    List _states = new List<CountryState>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.searchRead(Strings.res_country_state, [
            ['country_id', '=', 31]
          ], [
            'id',
            'name',
            'code',
            'country_id'
          ]).then(
            (OdooResponse res) {
              for (var i in res.getRecords()) {
                _states.add(
                  new CountryState(
                    id: i["id"],
                    name: i["name"],
                    code: i["code"],
                    countryId: i["country_id"][0],
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
//          List<Partner> lsPartner = await PartnerDao().findPartners();
//          _partners = lsPartner;
        }
      });
    });

    return _states;
  }
}
