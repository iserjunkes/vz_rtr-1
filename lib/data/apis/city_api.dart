import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class CityApi {
  static Future<List<City>> getCities(Base base) async {
    List _states = new List<City>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(Strings.res_city, [], ['id', 'name', 'state_id']).then(
            (OdooResponse res) {
              for (var i in res.getRecords()) {
                _states.add(
                  new City(
                    id: i["id"],
                    name: i["name"],
                    stateId: i["state_id"][0],
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
//          List<Partner> lsPartner = await PartnerDao().findPartners();
//          _partners = lsPartner;
        }
      });
    });

    return _states;
  }
}
