import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/blocs/campaign_bloc.dart';
import 'package:vorazrtr/data/blocs/city_bloc.dart';
import 'package:vorazrtr/data/blocs/partner_bloc.dart';
import 'package:vorazrtr/data/blocs/state_bloc.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class RtrApi {
  static Future<OdooResponse> saveRtr(Base base, Map values) async {
    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.create(Strings.vz_rtr, values).then(
            (OdooResponse res) async {
              //tratar resposta
              return res;
            },
          );
        } else {
//          List<Rtr> lsPartner = await PartnerDao().findPartners();
//          _rtrList = lsPartner;
        }
      });
    });
  }

  static Future<List<Rtr>> getRtrs(Base base) async {
    List _rtrList = new List<Rtr>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(
                  Strings.vz_rtr,
                  [
                    ['type', '=', 'rtr'],
                    ['is_rtr', '=', true]
                  ],
                  [
                    'name',
                    'report_name',
                    'campaign_id',
                    'partner_id',
                    'state_id',
                    'city_id'
                  ],
                  limit: 50)
              .then(
            (OdooResponse res) async {
              for (var i in res.getRecords()) {
//                String session = base.getSession().split(",")[0].split(";")[0];
                try {
                  Partner partner = await fullPartner(base, i);
                  Campaign campaign = await fullCampaign(base, i);
                  CountryState state = await fullState(base, i);
                  City city = await fullCity(base, i);

                  _rtrList.add(
                    new Rtr(
                      id: i["id"],
                      name: i["name"],
                      reportName: i["report_name"],
                      farm: partner,
                      campaign: campaign,
                      state: state,
                      city: city,
                    ),
                  );
                } catch (e) {
                  print("Erro de parse: $e");
                }
              }
            },
          );
        } else {
//          List<Rtr> lsPartner = await PartnerDao().findPartners();
//          _rtrList = lsPartner;
        }
      });
    });

    return _rtrList;
  }

  static Future<Partner> fullPartner(Base base, i) async {
    PartnerDao pDao = PartnerDao();
    Partner p = await pDao.findById(i["partner_id"][0]);
    if (p == null) {
      final _bloc = PartnerBloc();
      await _bloc.fetch(base);
      return await pDao.findById(i["partner_id"][0]);
    } else {
      return p;
    }
  }

  static Future<Campaign> fullCampaign(Base base, i) async {
    CampaignDao cDao = CampaignDao();
    Campaign c = await cDao.findById(i["campaign_id"][0]);
    if (c == null) {
      final _bloc = CampaignBloc();
      await _bloc.fetch(base);
      return await cDao.findById(i["campaign_id"][0]);
    } else {
      return c;
    }
  }

  static Future<CountryState> fullState(Base base, i) async {
    StateDao sDao = StateDao();
    CountryState s = await sDao.findById(i["state_id"][0]);
    if (s == null) {
      final _bloc = StateBloc();
      await _bloc.fetch(base);
      return await sDao.findById(i["state_id"][0]);
    } else {
      return s;
    }
  }

  static Future<City> fullCity(Base base, i) async {
    CityDao cityDao = CityDao();
    City c = await cityDao.findById(i["city_id"][0]);
    if (c == null) {
      final _bloc = CityBloc();
      await _bloc.fetch(base);
      return await cityDao.findById(i["city_id"][0]);
    } else {
      return c;
    }
  }
}
