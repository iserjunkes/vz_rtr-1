import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/utils/strings.dart';

class PartnerApi {
  static Future<List<Partner>> getPartners(Base base) async {
    List _partners = new List<Partner>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(
                  Strings.res_partner,
                  [
                    ["is_farm", "=", true]
                  ],
                  ['id', 'email', 'name', 'mobile'],
                  limit: 50)
              .then(
            (OdooResponse res) {
              for (var i in res.getRecords()) {
                String session = base.getSession().split(",")[0].split(";")[0];
                _partners.add(
                  new Partner(
                    id: i["id"],
                    email: i["email"] is! bool ? i["email"] : "N/A",
                    name: i["name"],
                    phone: i["mobile"] is! bool ? i["mobile"] : "N/A",
                    imageUrl: base.getURL() +
                        "/web/image?model=res.partner&field=image&" +
                        session +
                        "&id=" +
                        i["id"].toString(),
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
          List<Partner> lsPartner = await PartnerDao().findAll();
          _partners = lsPartner;
        }
      });
    });

    return _partners;
  }
}
