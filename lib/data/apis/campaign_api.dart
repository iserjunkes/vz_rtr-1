import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class CampaignApi {
  static Future<List<Campaign>> getCampaigns(Base base) async {
    List _campaigns = new List<Campaign>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(Strings.sale_campaign, [], ['id', 'name'], limit: 50)
              .then(
            (OdooResponse res) {
              for (var i in res.getRecords()) {
                _campaigns.add(
                  new Campaign(
                    id: i["id"],
                    name: i["name"],
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
//          List<Partner> lsPartner = await PartnerDao().findPartners();
//          _partners = lsPartner;
        }
      });
    });

    return _campaigns;
  }
}
