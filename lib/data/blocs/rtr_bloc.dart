import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class RtrBloc extends SimpleBloc<List<Rtr>> {
  Base base;

  Future<List<Rtr>> fetch(Base b) async {
    try {
      base = b;

      List<Rtr> rtrs = await RtrApi.getRtrs(base);

      final dao = RtrDao();

      rtrs.forEach((p) => dao.save(p));

      add(rtrs);

      return rtrs;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}
