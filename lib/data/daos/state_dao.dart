import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class StateDao extends BaseDAO<CountryState> {
  @override
  String get tableName => "state";

  @override
  CountryState fromMap(Map<String, dynamic> map) {
    return CountryState.fromMap(map);
  }

  Future<List<CountryState>> findStates() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }
}
