import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class CampaignDao extends BaseDAO<Campaign>{
  @override
  String get tableName => "campaign";

  @override
  Campaign fromMap(Map<String, dynamic> map) {
    return Campaign.fromMap(map);
  }

  Future<List<Campaign>> findCampaigns() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }

}