import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class PartnerDao extends BaseDAO<Partner>{
  @override
  String get tableName => "partner";

  @override
  Partner fromMap(Map<String, dynamic> map) {
    return Partner.fromMap(map);
  }

  Future<List<Partner>> findPartners() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }

}