import 'package:flutter/material.dart';

Text text(
    String s, {
      double fontSize = 16,
      color = Colors.white,
      bold = false
    }) {
  return Text(
    s ?? "",
    maxLines: 2,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(
      fontSize: fontSize,
      fontWeight: bold ? FontWeight.bold : FontWeight.normal,
    ),
  );
}