import 'package:flutter/material.dart';
import 'package:vorazrtr/drawer_list.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bem vindo ao Voraz!"),
      ),
      body: _body(),
      drawer: DrawerList(),
    );
  }

  _body() {
    return Center(
      child: Text(
        "Dashboards incoming...",
        style: TextStyle(fontSize: 24),
      ),
    );
  }
}
