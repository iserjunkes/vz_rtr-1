import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/user.dart';
import 'package:vorazrtr/data/services/odoo_api.dart';
import 'package:vorazrtr/pages/home_page.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';
import 'package:vorazrtr/widgets/app_button.dart';
import 'package:vorazrtr/widgets/app_textfield.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends Base<LoginPage> {
  final _tLogin = TextEditingController();
  final _tPassword = TextEditingController();
  final _tUrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _focusPassword = FocusNode();
  final _focusEmail = FocusNode();

  bool _showProgress = false;
  String odooURL;
  String _selectedDb;
  List<String> _dbList = [];
  List dynamicList = [];
  bool isCorrectURL = false;
  bool isDBFilter = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _checkFirstTime();
    });
  }

  _checkFirstTime() {
    if (getURL() != null) {
      odooURL = getURL();
    }
  }

  _checkUrl(login, password) {
    isConnected().then((isInternet) {
      if (isInternet) {
//        showLoading();
        // Init Odoo URL when URL is not saved
        odoo = new Odoo(url: odooURL);
        print("URL $odooURL");
        odoo.getDatabases().then((http.Response res) {
          setState(
            () {
              print("Odoo Server Connected");
//              hideLoadingSuccess();
              isCorrectURL = true;
              dynamicList = json.decode(res.body)['result'] as List;
              saveOdooUrl(odooURL);
              dynamicList.forEach((db) => _dbList.add(db));
              _selectedDb = _dbList[0];
              if (_dbList.length == 1) {
                isDBFilter = true;
              } else {
                isDBFilter = false;
              }
              _login(login, password);
            },
          );
        }).catchError(
          (e) {
            alert(context, "URL invalida!", hideCancelBtn: true);
            _setState(false);
            print("Erro ==> " + e.toString());
          },
        );
      }
    });
  }

  _body() {
    return Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            _imgLogo(),
            AppTextfield(
              "URL",
              invisible: false,
              controller: _tUrl,
              textInputAction: TextInputAction.next,
              nextFocus: _focusEmail,
              validator: _validRequired,
            ),
            SizedBox(height: 10),
            AppTextfield(
              "Login",
              invisible: false,
              controller: _tLogin,
              textInputAction: TextInputAction.next,
              focusNode: _focusEmail,
              nextFocus: _focusPassword,
              validator: _validRequired,
            ),
            SizedBox(height: 10),
            AppTextfield(
              "Senha",
              invisible: true,
              controller: _tPassword,
              focusNode: _focusPassword,
              validator: _validRequired,
            ),
            SizedBox(height: 30),
            AppButton(
              "Login",
              onPressed: _onClickLogin,
              showProgress: _showProgress,
            )
          ],
        ),
      ),
    );
  }

  _imgLogo() {
    return Container(
      margin: EdgeInsets.all(30),
      child: Image.asset(
        "assets/images/voraz_agtech.png",
        fit: BoxFit.contain,
      ),
    );
  }

  void _onClickLogin() {
    bool formOk = _formKey.currentState.validate();

    if (!formOk) {
      return;
    }

    _setState(true);

    String url = _tUrl.text;
    String login = _tLogin.text;
    String password = _tPassword.text;

    if (url.substring(0, 7) == "http://") {
      url = url.replaceRange(0, 7, "https://");
      odooURL = url;
    } else if (url.substring(0, 8) == "https://") {
      odooURL = url;
    } else {
      odooURL = "https://" + url;
    }
    _checkUrl(login, password);
  }

  void _login(String login, String password) {
    print("Login proccess started!");
    isConnected().then((isInternet) {
      if (isInternet) {
//        showLoading();
        odoo.authenticate(login, password, _selectedDb).then(
          (http.Response auth) {
            if (auth.body != null && !auth.body.contains("Access denied")) {
//              hideLoadingDialog();
              User user = User.fromJson(jsonDecode(auth.body));
              saveUser(json.encode(user));

              setSessionId(user.result.sessionId);
              saveOdooUrl(odooURL);
              push(context, HomePage(), replace: true);
            } else {
              _setState(false);
              alert(context, "Usuário e senha inválidos.", hideCancelBtn: true);
            }
          },
        );
      }
    });
  }

  String _validRequired(String value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }

  _setState(bool showProgress) {
    setState(() {
      _showProgress = showProgress;
    });
  }
}
