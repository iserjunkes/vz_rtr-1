import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/utils/my_text.dart';

class PartnerPage extends StatelessWidget {
  Partner partner;

  PartnerPage(this.partner);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(partner.name),
      ),
      body: _body(),
    );
  }

  _body() {
    print(partner.imageUrl);
    return Container(
      padding: EdgeInsets.all(35),
      child: ListView(
        children: <Widget>[
          CachedNetworkImage(
              imageUrl: partner.imageUrl ??
                  "https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png"),
          _partnerDetails(),
          Divider(),
//          _partnerVisits(),
        ],
      ),
    );
  }

  _partnerDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
            child: Column(
          children: <Widget>[
            text(partner.name, fontSize: 20, bold: true),
            text("E-mail: ${partner.email}", fontSize: 16),
            text("Fone: ${partner.phone}", fontSize: 16),
            text("Endereço: ${partner.address != null ? partner.address : "N/A"}", fontSize: 16),
          ],
        ))
      ],
    );
  }
}
